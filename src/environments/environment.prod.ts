export const environment = {
  production: true,
  api: {
    base: 'http://192.168.1.142:8080/mimacs/rest/',
    security: 'http://localhost:8080/rest/securitymanager/',
    resource: 'http://localhost:8081/resourcemanager/',
  }
};
