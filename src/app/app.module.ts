import { BrowserModule } from '@angular/platform-browser';
import {isDevMode, NgModule} from '@angular/core';

import { AppRoutingModule } from './app-routing.module';

import { ServiceWorkerModule } from '@angular/service-worker';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment';
import { NavbarComponent } from './navbar/navbar.component';
import {SharedModule} from './shared/shared.module';
import { HomeComponent } from './home/home.component';
import {LampsmanModule} from './lampsman/lampsman.module';
import {DevToolsExtension, NgRedux} from '@angular-redux/store';
import {IAppState, INITIAL_STATE, rootReducer} from './app.store';
import {AssetService} from './_services/asset.service';
import {LookupService} from './_services/lookup.service';
import {PersonService} from './_services/person.service';
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {RequestInterceptor} from './_inteceptors/request.interceptor';
import {AuthGuard} from './_guards/auth.guard';
import {AuthChildrenGuard} from './_guards/auth-children.guard';
import {AuthenticationService} from './_services/authentication.service';
import {SystemService} from './_services/system.service';
import {UsersModule} from './users/users.module';
import {ReportsModule} from './reports/reports.module';
import {LocationService} from './_services/location.service';
import {EventsModule} from './events/events.module';
import {MovementService} from './_services/movement.service';
import {NgHttpLoaderModule} from 'ng-http-loader';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    ServiceWorkerModule.register('/webapp/ngsw-worker.js', { enabled: environment.production }),
    SharedModule,
    LampsmanModule,
    UsersModule,
    ReportsModule,
    EventsModule,
    AppRoutingModule,
    NgHttpLoaderModule,
    // AngularHalModule
  ],
  providers: [
    AssetService,
    AuthenticationService,
    LocationService,
    LookupService,
    MovementService,
    PersonService,
    SystemService,
    AuthGuard,
    AuthChildrenGuard,
    { provide: HTTP_INTERCEPTORS, useClass: RequestInterceptor, multi: true },
    // { provide: 'ExternalConfigurationService', useClass: ExternalConfigurationService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(ngRedux: NgRedux<IAppState>, devTools: DevToolsExtension) {
    const enhancers: any = isDevMode() ? [devTools.enhancer()] : [];
    ngRedux.configureStore(rootReducer, INITIAL_STATE, [], enhancers);
  }
}
