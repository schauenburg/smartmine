import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sm-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  menuTitle = 'Smart Mine';
  constructor() { }

  ngOnInit() {
  }

  setMenuTitle(title: string): void {
    this.menuTitle = title;
  }

}
