import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {Assignment} from '../../../_models/assignment';
import {Person} from '../../../_models/person';
import {IAssetType} from '../../../_models/asset-type';

@Component({
  selector: 'sm-misc',
  templateUrl: './misc.component.html',
  styleUrls: ['./misc.component.css']
})
export class MiscComponent implements OnChanges {
  equipmentState = 'misc unassigned';
  @Input() assignedMisc: Assignment[];
  assetTypes: IAssetType[];
  constructor() { }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    if (changes['assignedMisc'] && this.assignedMisc.length > 0) {
      this.equipmentState = 'misc assigned';
    }
    if (changes['assignedMisc'] && this.assignedMisc.length === 0) {
      this.equipmentState = 'misc unassigned';
    }
  }

  onClick() {}
}
