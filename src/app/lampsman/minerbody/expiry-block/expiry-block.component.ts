import { Component, OnInit } from '@angular/core';
import {Person} from '../../../_models/person';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../app.store';

@Component({
  selector: 'sm-expiry-block',
  templateUrl: './expiry-block.component.html',
  styleUrls: ['./expiry-block.component.css']
})
export class ExpiryBlockComponent implements OnInit {
  personStatus: any;
  person: Person;
  constructor(private ngRedux: NgRedux<IAppState>) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.person = store.lampsman.selectedPerson;
    });
  }

  ngOnInit() {
  }

  addInductionExpiry() {}
  addMedicalExpiry() {}
  addTrainingExpiry() {}
}
