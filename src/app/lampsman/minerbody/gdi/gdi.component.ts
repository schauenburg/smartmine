import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {Assignment} from '../../../_models/assignment';
import {Person} from '../../../_models/person';
import {AssetService} from '../../../_services/asset.service';
import {IAssetType} from '../../../_models/asset-type';

@Component({
  selector: 'sm-gdi',
  templateUrl: './gdi.component.html',
  styleUrls: ['./gdi.component.css']
})
export class GdiComponent implements OnInit, OnChanges {
  equipmentState = 'gdi unassigned';
  @Input() assignedGdis: Assignment[];
  assetTypes: IAssetType[];

  constructor(private assetService: AssetService) { }

  ngOnInit() {
    this.assetService.getAssetTypesByClass('gas-detector')
      .subscribe(response => {
        this.assetTypes = response;
      });
  }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    if (changes['assignedGdis'] && this.assignedGdis.length > 0) {
      this.equipmentState = 'gdi assigned';
    }
    if (changes['assignedGdis'] && this.assignedGdis.length === 0) {
      this.equipmentState = 'gdi unassigned';
    }
  }

}
