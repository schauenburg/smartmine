import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {Assignment} from '../../../_models/assignment';
import {AssetService} from '../../../_services/asset.service';
import {IAssetType} from '../../../_models/asset-type';

@Component({
  selector: 'sm-caplamp',
  templateUrl: './caplamp.component.html',
  styleUrls: ['./caplamp.component.css']
})
export class CaplampComponent implements OnInit, OnChanges {
  equipmentState = 'cap-lamp unassigned';
  @Input() assignedCaplamps: Assignment[];
  assetTypes: IAssetType[];

  constructor(private assetService: AssetService) {}

  ngOnInit() {
    this.assetService.getAssetTypesByClass('cap-lamp')
      .subscribe(response => {
        this.assetTypes = response;
      });
  }

  ngOnChanges(changes: { [propertyName: string]: SimpleChange }): void {
    if (changes['assignedCaplamps'] && this.assignedCaplamps.length > 0) {
      this.equipmentState = 'cap-lamp assigned';
    }
    if (changes['assignedCaplamps'] && this.assignedCaplamps.length === 0) {
      this.equipmentState = 'cap-lamp unassigned';
    }
  }

}
