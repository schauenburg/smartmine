import {Component, Input, OnChanges, OnInit, SimpleChange} from '@angular/core';
import {Assignment} from '../../../_models/assignment';
import {Person} from '../../../_models/person';
import {IAssetType} from '../../../_models/asset-type';

@Component({
  selector: 'sm-pds',
  templateUrl: './pds.component.html',
  styleUrls: ['./pds.component.css']
})
export class PdsComponent implements OnChanges {
  equipmentState = 'pds unassigned';
  @Input() assignedPds: Assignment[];
  assetTypes: IAssetType[];
  constructor() { }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    if (changes['assignedPds'] && this.assignedPds.length > 0) {
      this.equipmentState = 'pds assigned';
    }
    if (changes['assignedPds'] && this.assignedPds.length === 0) {
      this.equipmentState = 'pds unassigned';
    }
  }

  onClick() {}
}
