import {Asset} from '../_models/asset';
import {IAssetType} from '../_models/asset-type';
import {Person} from '../_models/person';
import {IPersonType} from '../_models/person-type';
import {Assignment} from '../_models/assignment';
import {tassign} from 'tassign';
import {
  ADD_RECENTLY_ALLOCATED_ASSET,
  ADD_RECENTLY_CREATED_ASSET,
  ADD_RECENTLY_CREATED_PERSON,
  ADD_RECENTLY_DEALLOCATED_ASSET, ADD_TO_ASSET_SEARCH_RESULTS,
  CLEAR_SEARCH,
  DISPLAY_ASSET_ELEMENTS,
  DISPLAY_PERSON_ELEMENTS,
  FETCH_ALL_PERSONS,
  FETCH_ASSETSTATUSES_ERROR,
  FETCH_ASSETSTATUSES_REQUEST,
  FETCH_ASSETSTATUSES_SUCCESS,
  FETCH_ASSETTYPES_REQUEST,
  FETCH_ASSETTYPES_SUCCESS,
  FETCH_PERSONTYPES_REQUEST,
  FETCH_PERSONTYPES_SUCCESS, REMOVE_FROM_ASSET_SEARCH_RESULTS,
  REMOVE_RECENTLY_ALLOCATED_ASSET,
  REMOVE_RECENTLY_CREATED_ASSET,
  REMOVE_RECENTLY_CREATED_PERSON,
  REMOVE_RECENTLY_DEALLOCATED_ASSET,
  SEARCH_ASSET_ERROR,
  SEARCH_ASSET_REQUEST,
  SEARCH_ASSET_SUCCESS,
  SEARCH_PERSON_ERROR,
  SEARCH_PERSON_REQUEST,
  SEARCH_PERSON_SUCCESS,
  SELECT_PERSON,
  SHOW_ASSET_CREATE_FORM,
  SHOW_ASSETSEARCH_RESULTS,
  SHOW_PERSON_CREATE_FORM,
  SHOW_PERSON_INFOR,
  SHOW_PERSONSEARCH_RESULTS,
  UPDATE_ACTIVE_TAG_READER_LIST,
  UPDATE_ASSET_SEARCH_RESULTS,
  UPDATE_CLASS_ASSET_TYPES,
  UPDATE_PASSIVE_TAG_READER_LIST,
  UPDATE_PERSON_ASSIGNMENTS,
  UPDATE_PERSON_INFORMATION_FOCUS,
  UPDATE_PERSONSEARCH_RESULTS,
  UPDATE_SELECTED_ACTIVE_TAG_READER,
  UPDATE_SELECTED_ASSET,
  UPDATE_SELECTED_ASSET_TYPE,
  UPDATE_SELECTED_ASSET_TYPE_ATTRIBUTES,
  UPDATE_SELECTED_PASSIVE_TAG_READER,
  UPDATE_SELECTED_PERSON,
  UPDATE_SELECTED_PERSON_TYPE,
  UPDATE_SELECTED_PERSON_TYPE_ATTRIBUTES
} from './lampsman.action';
import {IAttribute} from '../_models/attribute';
import {Location} from '../_models/location';

export interface ILampsmanState {
  selectedAsset: Asset;
  selectedAssetType: IAssetType;
  selectedAssetTypeAttributes: IAttribute[];
  showAssetInformation: boolean;
  isSearchingAssets: boolean;
  assetSearchResults: Asset[];
  showAssetSearchResults: boolean;
  assetTypes: IAssetType[];
  assetClassTypes: IAssetType[];
  isFetchingAssetTypes: boolean;
  assetStatuses: any[];
  isFetchingAssetStatuses: boolean;
  showAssetElements: boolean;
  showAssetCreateForm: boolean;
  recentlyCreatedAssets: Asset[];
  recentlyAllocatedAssets: Asset[];
  recentlyDeallocatedAssets: Asset[];
  allAssets: Asset[];

  selectedPerson: Person;
  selectedPersonType: IPersonType;
  selectedPersonTypeAttributes: IAttribute[];
  showPersonInformation: boolean;
  personInformationFocus: boolean;
  isSearchingPersons: boolean;
  personSearchResults: Person[];
  showPersonSearchResults: boolean;
  personTypes: IPersonType[];
  isFetchingPersonTypes: boolean;
  personAssignments: Assignment[];
  showPersonCreateForm: boolean;
  showPersonElements: boolean;
  recentlyCreatedPersons: Person[];
  allPersons: Person[];

  // lookups: any[];
  // locations
  passiveTagReaderList: Location[];
  activeTagReaderList: Location[];
  selectedPassiveTagReader: Location;
  selectedActiveTagReader: Location;
}

export const LAMPSMAN_INITIAL_STATE: ILampsmanState = {
  selectedAsset: undefined,
  selectedAssetType: undefined,
  selectedAssetTypeAttributes: [],
  showAssetInformation: false,
  isSearchingAssets: false,
  assetSearchResults: [],
  showAssetSearchResults: false,
  assetTypes: [],
  assetClassTypes: [],
  isFetchingAssetTypes: false,
  assetStatuses: [],
  isFetchingAssetStatuses: false,
  showAssetElements: true,
  recentlyCreatedAssets: [],
  selectedPerson: undefined,
  selectedPersonType: undefined,
  selectedPersonTypeAttributes: [],
  showPersonInformation: false,
  personInformationFocus: false,
  isSearchingPersons: false,
  personSearchResults: [],
  showPersonSearchResults: false,
  personTypes: [],
  isFetchingPersonTypes: false,
  personAssignments: [],
  showPersonElements: false,
  showAssetCreateForm: false,
  showPersonCreateForm: false,
  recentlyCreatedPersons: [],
  recentlyAllocatedAssets: [],
  recentlyDeallocatedAssets: [],
  allAssets: [],
  allPersons: [],
  activeTagReaderList: [],
  passiveTagReaderList: [],
  selectedActiveTagReader: undefined,
  selectedPassiveTagReader: undefined
};

function fetchAssetStatusesRequest(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {isFetchingAssetStatuses: true});
}

function fetchAssetStatusesSuccess(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {isFetchingAssetStatuses: false, assetStatuses: action.assetStatuses});
}

function fetchAssetStatusesError(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {isFetchingAssetStatuses: false});
}

function fetchAssetTypesRequest(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {isFetchingAssetTypes: true});
}

function fetchAssetTypesSuccess(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {assetTypes: action.assetTypes, isFetchingAssetTypes: false});
}

function searchAssetsRequest(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {isSearchingAssets: true});
}

function searchAssetsSuccess(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {
    isSearchingAssets: false,
    assetSearchResults: action.assetSearchResults,
    showAssetSearchResults: true,
  });
}

function searchAssetsError(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {isSearchingAssets: false});
}

function updateSelectedAsset(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {selectedAsset: action.selectedAsset, showAssetInformation: true});
}

function updateSelectedAssetType(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {selectedAssetType: action.assetType});
}

function updateSelectedAssetTypeAttributes(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {selectedAssetTypeAttributes: action.attributes});
}

function updateAssetClassTypes(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {assetClassTypes: action.types});
}

function showAssetSearchResults(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {showAssetSearchResults: action.show});
}

function showAssetElements(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {showAssetElements: action.show});
}

function showPersonElements(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {showPersonElements: action.show});
}

function fetchPersonTypesRequest(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {isFetchingPersonTypes: true});
}

function fetchPersonTypesSuccess(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {personTypes: action.personTypes});
}

function searchPersonsRequest(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {isSearchingPersons: true});
}

function searchPersonsSuccess(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {
    isSearchingPersons: false,
    personSearchResults: action.personSearchResults,
    showPersonSearchResults: true,
  });
}

function searchPersonsError(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {isSearchingPersons: false});
}

function selectPerson(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {selectedPerson: action.selectedPerson, showPersonInformation: true});
}

function showPersonInformation(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {showPersonInformation: action.show});
}

function showPersonSearchResults(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {showPersonSearchResults: action.show});
}

function updatePersonSearchResults(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {personSearchResults: action.personSearchResults});
}

function updateSelectedPerson(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {selectedPerson: action.selectedPerson});
}

function updateSelectedPersonType(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {selectedPersonType: action.personType});
}

function updateSelectedPersonTypeAttributes(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {selectedPersonTypeAttributes: action.attributes});
}

function updatePersonAssignments(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {personAssignments: action.assignments});
}

function addRecentlyCreatedAsset(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {recentlyCreatedAssets: state.recentlyCreatedAssets.concat(action.asset)});
}

function removeRecentlyCreatedAsset(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {recentlyCreatedAssets: state.recentlyCreatedAssets.filter(asset => asset.assetId !== action.asset.assetId)});
}

function addRecentlyAllocatedAsset(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {recentlyAllocatedAssets: state.recentlyAllocatedAssets.concat(action.asset)});
}

function addToAssetSearchResults(state: ILampsmanState, action: any): ILampsmanState {
  // tassign(state, {assetSearchResults: state.assetSearchResults.filter(asset => asset.assetId !== action.asset.assetId)});
   return tassign(state, {
     assetSearchResults: state.assetSearchResults.concat(action.asset)
   });
}

function removeFromAssetSearchResults(state: ILampsmanState, action: any): ILampsmanState {
  // tassign(state, {assetSearchResults: state.assetSearchResults.filter(asset => asset.assetId !== action.asset.assetId)});
  return tassign(state, {
    assetSearchResults: state.assetSearchResults.filter(asset => asset.assetId !== action.asset.assetId)
  });
}

function removeRecentlyAllocatedAsset(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {recentlyAllocatedAssets: state.recentlyAllocatedAssets.filter(asset => asset.assetId !== action.asset.assetId)});
}

function addRecentlyDeallocatedAsset(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {recentlyDeallocatedAssets: state.recentlyDeallocatedAssets.concat(action.asset)});
}

function removeRecentlyDeallocatedAsset(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {
    recentlyDeallocatedAssets: state.recentlyDeallocatedAssets.filter(asset => asset.assetId !== action.asset.assetId)});
}

function addRecentlyCreatedPerson(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {recentlyCreatedPersons: state.recentlyCreatedPersons.concat(action.person)});
}

function removeRecentlyCreatedPerson(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {
    recentlyCreatedPersons: state.recentlyCreatedPersons.filter(person => person.personId !== action.person.personId)});
}

function showAssetCreateForm(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {showAssetCreateForm: action.show});
}

function showPersonCreateForm(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {showPersonCreateForm: action.show});
}

function fetchAllPersons(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {allPersons: action.persons});
}

function clearSearch(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {
    personSearchResults: [],
    assetSearchResults: [],
    selectedAsset: undefined,
    selectedPerson: undefined,
    selectedAssetType: undefined,
    selectedPersonType: undefined,
    personAssignments: [],
    showPersonSearchResults: false,
    showPersonInformation: false,
    personInformationFocus: false
  });
}

function updatePersonInformationFocus(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {personInformationFocus: action.focus});
}

function updateActiveTagReaderList(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {activeTagReaderList: action.readers});
}

function updatePassiveTagReaderList(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {passiveTagReaderList: action.readers});
}

function updateSelectedPassiveTagReader(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {selectedPassiveTagReader: action.reader});
}

function updateSelectedActiveTagReader(state: ILampsmanState, action: any): ILampsmanState {
  return tassign(state, {selectedActiveTagReader: action.reader});
}

export function lampsmanReducer(state: ILampsmanState = LAMPSMAN_INITIAL_STATE, action: any): ILampsmanState {
  switch (action.type) {
    case ADD_RECENTLY_CREATED_ASSET:
      return addRecentlyCreatedAsset(state, action);
    case REMOVE_RECENTLY_CREATED_ASSET:
      return removeRecentlyCreatedAsset(state, action);
    case ADD_RECENTLY_ALLOCATED_ASSET:
      return addRecentlyAllocatedAsset(state, action);
    case REMOVE_RECENTLY_ALLOCATED_ASSET:
      return removeRecentlyAllocatedAsset(state, action);
    case ADD_RECENTLY_DEALLOCATED_ASSET:
      return addRecentlyDeallocatedAsset(state, action);
    case REMOVE_RECENTLY_DEALLOCATED_ASSET:
      return removeRecentlyDeallocatedAsset(state, action);
    case ADD_RECENTLY_CREATED_PERSON:
      return addRecentlyCreatedPerson(state, action);
    case REMOVE_RECENTLY_CREATED_PERSON:
      return removeRecentlyCreatedPerson(state, action);
    case FETCH_ASSETSTATUSES_ERROR:
      return fetchAssetStatusesError(state, action);
    case FETCH_ASSETSTATUSES_REQUEST:
      return fetchAssetStatusesRequest(state, action);
    case FETCH_ASSETSTATUSES_SUCCESS:
      return fetchAssetStatusesSuccess(state, action);
    case  FETCH_ASSETTYPES_REQUEST:
      return fetchAssetTypesRequest(state, action);
    case  FETCH_ASSETTYPES_SUCCESS:
      return fetchAssetTypesSuccess(state, action);
    case SEARCH_ASSET_REQUEST:
      return searchAssetsRequest(state, action);
    case SEARCH_ASSET_SUCCESS:
      return searchAssetsSuccess(state, action);
    case SEARCH_ASSET_ERROR:
      return searchAssetsError(state, action);
    case UPDATE_SELECTED_ASSET:
      return updateSelectedAsset(state, action);
    case UPDATE_SELECTED_ASSET_TYPE:
      return updateSelectedAssetType(state, action);
    case UPDATE_SELECTED_ASSET_TYPE_ATTRIBUTES:
      return updateSelectedAssetTypeAttributes(state, action);
    case UPDATE_CLASS_ASSET_TYPES:
      return updateAssetClassTypes(state, action);
    case SHOW_ASSETSEARCH_RESULTS:
      return showAssetSearchResults(state, action);
    case ADD_TO_ASSET_SEARCH_RESULTS:
      return addToAssetSearchResults(state, action);
    case REMOVE_FROM_ASSET_SEARCH_RESULTS:
      return removeFromAssetSearchResults(state, action);
    case DISPLAY_ASSET_ELEMENTS:
      return showAssetElements(state, action);
    case  FETCH_PERSONTYPES_REQUEST:
      return fetchPersonTypesRequest(state, action);
    case  FETCH_PERSONTYPES_SUCCESS:
      return fetchPersonTypesSuccess(state, action);
    case FETCH_ALL_PERSONS:
      return fetchAllPersons(state, action);
    case SEARCH_PERSON_REQUEST:
      return searchPersonsRequest(state, action);
    case SEARCH_PERSON_SUCCESS:
      return searchPersonsSuccess(state, action);
    case SEARCH_PERSON_ERROR:
      return searchPersonsError(state, action);
    case UPDATE_SELECTED_PERSON:
      return updateSelectedPerson(state, action);
    case UPDATE_SELECTED_PERSON_TYPE:
      return updateSelectedPersonType(state, action);
    case UPDATE_SELECTED_PERSON_TYPE_ATTRIBUTES:
      return updateSelectedPersonTypeAttributes(state, action);
    case SHOW_PERSON_INFOR:
      return showPersonInformation(state, action);
    case SHOW_PERSONSEARCH_RESULTS:
      return showPersonSearchResults(state, action);
    case UPDATE_PERSONSEARCH_RESULTS:
      return updatePersonSearchResults(state, action);
    case UPDATE_PERSON_ASSIGNMENTS:
      return updatePersonAssignments(state, action);
    case DISPLAY_PERSON_ELEMENTS:
      return showPersonElements(state, action);
    case SHOW_ASSET_CREATE_FORM:
      return showAssetCreateForm(state, action);
    case SHOW_PERSON_CREATE_FORM:
      return showPersonCreateForm(state, action);
    case CLEAR_SEARCH:
      return clearSearch(state, action);
    case UPDATE_PERSON_INFORMATION_FOCUS:
      return updatePersonInformationFocus(state, action);
    case UPDATE_ACTIVE_TAG_READER_LIST:
      return updateActiveTagReaderList(state, action);
    case UPDATE_PASSIVE_TAG_READER_LIST:
      return updatePassiveTagReaderList(state, action);
    case UPDATE_SELECTED_ACTIVE_TAG_READER:
      return updateSelectedActiveTagReader(state, action);
    case UPDATE_SELECTED_PASSIVE_TAG_READER:
      return updateSelectedPassiveTagReader(state, action);
    default:
      return state;
  }
}
