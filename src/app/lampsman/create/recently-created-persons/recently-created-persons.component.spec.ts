import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentlyCreatedPersonsComponent } from './recently-created-persons.component';

describe('RecentlyCreatedPersonsComponent', () => {
  let component: RecentlyCreatedPersonsComponent;
  let fixture: ComponentFixture<RecentlyCreatedPersonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentlyCreatedPersonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentlyCreatedPersonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
