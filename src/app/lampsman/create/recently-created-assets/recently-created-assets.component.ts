import { Component, OnInit } from '@angular/core';
import {Observable} from 'rxjs';
import {select} from '@angular-redux/store';
import {Asset} from '../../../_models/asset';

@Component({
  selector: 'sm-recently-created-assets',
  templateUrl: './recently-created-assets.component.html',
  styleUrls: ['./recently-created-assets.component.css']
})
export class RecentlyCreatedAssetsComponent implements OnInit {
  @select(s => s.lampsman.showAssetElements) showCard: Observable<boolean>;
  @select(s => s.lampsman.recentlyCreatedAssets) assets: Observable<Asset[]>;
  constructor() { }

  ngOnInit() {
  }

}
