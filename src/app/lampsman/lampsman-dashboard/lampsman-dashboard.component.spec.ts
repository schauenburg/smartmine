import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LampsmanDashboardComponent } from './lampsman-dashboard.component';

describe('LampsmanDashboardComponent', () => {
  let component: LampsmanDashboardComponent;
  let fixture: ComponentFixture<LampsmanDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LampsmanDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LampsmanDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
