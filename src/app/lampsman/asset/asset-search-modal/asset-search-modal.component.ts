import {Component, OnInit, ViewChild} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {FormControl, FormGroup} from '@angular/forms';
import {IAssetType} from '../../../_models/asset-type';
import {NgRedux, select} from '@angular-redux/store';
import {Asset} from '../../../_models/asset';
import {Observable} from 'rxjs/index';
import {IAttribute} from '../../../_models/attribute';
import {AssetService} from '../../../_services/asset.service';
import {SEARCH_ASSET_SUCCESS} from '../../lampsman.action';
import {LookupService} from '../../../_services/lookup.service';
import {IAppState} from '../../../app.store';
import {ToastrService} from 'ngx-toastr';
import {MatPaginator, MatSort} from '@angular/material';
import {AssetSearchResultsDataSource} from '../../search/search-asset/asset-search-results/asset-search-results-datasource';

@Component({
  selector: 'sm-asset-search-modal',
  templateUrl: './asset-search-modal.component.html',
  styleUrls: ['./asset-search-modal.component.css']
})
export class AssetSearchModalComponent implements OnInit {
  searchAssetForm: FormGroup;
  simpleView = true;
  advancedView = false;
  statusOptions: any[];

  @select(s => s.lampsman.assetTypes) assetTypes: Observable<IAssetType>;
  assetTypeAttributes: IAttribute[];
  @select(s => s.lampsman.assetSearchResults) assetSearchResults: Observable<Asset[]>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: AssetSearchResultsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['assetId', 'type', 'status', 'allocStatus', 'options'];

  constructor(public activeModal: NgbActiveModal,
              private lookupService: LookupService,
              private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService,
              private assetService: AssetService) {
  }

  ngOnInit() {
    this.assetService.fetchAssetTypes();
    this.fetchStatusOptions();
    this.initSearchAssetForm();
    this.dataSource = new AssetSearchResultsDataSource(this.paginator, this.sort);
  }

  private initSearchAssetForm() {
    this.searchAssetForm = new FormGroup({
      assetId: new FormControl(),
      assetType: new FormControl(),
      attributeName: new FormControl(),
      attributeValue: new FormControl(),
      maxResults: new FormControl(),
      status: new FormControl(),
      spareIndicator: new FormControl(),
      tracked: new FormControl(),
      allocated: new FormControl(),
      tagged: new FormControl(),
      shift: new FormControl()
    });
  }

  selectAsset(asset: Asset) {
    this.activeModal.close({success: true, assetId: asset.assetId});
  }

  toggleSimpleAdvanced() {
    this.simpleView = !this.simpleView;
    this.advancedView = !this.advancedView;
  }

  onTypeChanged(type: any) {
    this.assetService.getAssetTypeAttributes(type)
      .subscribe(response => {
        this.assetTypeAttributes = response;
      });
  }

  searchAsset(formValue: any) {
    if (!formValue.maxResults) {
      formValue.maxResults = 100;
    }
    this.assetService.searchAsset(formValue)
      .subscribe(response => {
          if (response._embedded !== undefined) {
            const result = response._embedded['assetResources'];
            if (result.length > 0) {
              this.ngRedux.dispatch({type: SEARCH_ASSET_SUCCESS, assetSearchResults: result});
            }
          } else {
            this.toastr.info('No asset matched your search criteria', 'Asset not found');
            this.ngRedux.dispatch({type: SEARCH_ASSET_SUCCESS, assetSearchResults: []});
          }
        },
        error2 => {
          console.log(error2);
          this.toastr.error('There was an error searching for Assets!', 'Search Error');
          this.ngRedux.dispatch({type: SEARCH_ASSET_SUCCESS, assetSearchResults: []});
        });
  }

  private fetchStatusOptions() {
    this.lookupService.getLookupList('assetStatus')
      .subscribe(response => {
        this.statusOptions = response;
      });
  }
}
