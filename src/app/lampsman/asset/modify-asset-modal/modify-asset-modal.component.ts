import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Asset} from '../../../_models/asset';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AssetService} from '../../../_services/asset.service';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../app.store';
import {ToastrService} from 'ngx-toastr';
import {AttributeTransformPipe} from '../../../_pipes/attribute-transform.pipe';
import {IAttributeMapEntry} from '../../../_models/attribute-map-entry';

@Component({
  selector: 'sm-modify-asset-modal',
  templateUrl: './modify-asset-modal.component.html',
  styleUrls: ['./modify-asset-modal.component.css']
})
export class ModifyAssetModalComponent implements OnInit {
  @Input() asset: Asset;
  editAssetForm: FormGroup;

  constructor(public activeModal: NgbActiveModal,
              private ngRedux: NgRedux<IAppState>,
              private formBuilder: FormBuilder,
              private assetService: AssetService,
              private toastr: ToastrService) { }

  ngOnInit() {
    this.initFormModel();
  }

  initFormModel(): void {
    this.editAssetForm = this.formBuilder.group({
      assetId: [this.asset.assetId, [Validators.required]],
      type: [this.asset.assetType],
      status: [this.asset.status],
      allocStatus: [this.asset.allocStatus],
      attributeMap: this.attributeMapFormGroup(),
    });
  }

  attributeMapFormGroup(): FormGroup {
    const attribFormGroup = new FormGroup({});
    console.log('asset looks like: ', this.asset);
    for (const attrib of this.asset.attr) {
      attribFormGroup.addControl(attrib.name, new FormControl(attrib.value));
    }
    return attribFormGroup;
  }

  modifyAsset(form: any) {
    const map: any[] = <IAttributeMapEntry[]>new AttributeTransformPipe().transform(form.attributeMap);
    form.attributeMap = map;
    form.type = this.asset.type.name;
    this.assetService.editAsset(form)
      .subscribe(response => {
          this.activeModal.close({success: true, asset: response});
          this.toastr.success('Successfully modified equipment', 'Success');
        },
        error2 => {
          this.toastr.error('Sorry, equipment could not be modified', 'Error');
          this.activeModal.close({success: false, error: error2});
        });
  }
}
