import {AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChange, ViewChild} from '@angular/core';
import {NgRedux, select} from '@angular-redux/store';
import {Observable, Subject} from 'rxjs';
import {Assignment} from '../../../_models/assignment';
import {DeallocateConfirmationModalComponent} from '../../allocate/deallocate-confirmation-modal/deallocate-confirmation-modal.component';
import {ADD_RECENTLY_DEALLOCATED_ASSET, UPDATE_PERSON_ASSIGNMENTS} from '../../lampsman.action';
import {IAppState} from '../../../app.store';
import {ToastrService} from 'ngx-toastr';
import {AssetService} from '../../../_services/asset.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {DataTableDirective} from 'angular-datatables';
import {Router} from '@angular/router';
import {Asset} from '../../../_models/asset';
import {AssetDetailsModalComponent} from '../../asset/asset-details-modal/asset-details-modal.component';
import {ModifyAssetModalComponent} from '../../asset/modify-asset-modal/modify-asset-modal.component';

@Component({
  selector: 'sm-person-allocations',
  templateUrl: './person-allocations.component.html',
  styleUrls: ['./person-allocations.component.css']
})
export class PersonAllocationsComponent implements OnInit, OnChanges, AfterViewInit {
  @select(s => s.lampsman.personAssignments) assignmentsObserver: Observable<Assignment[]>;
  @Input() personAssignments: Assignment[];
  @ViewChild(DataTableDirective) dtElement: DataTableDirective;
  dtOptions: DataTables.Settings = {};
  // We use this trigger because fetching the list of persons can be quite long,
  // thus we ensure the data is fetched before rendering
  dtTrigger: Subject<any> = new Subject();

  focus: boolean;

  constructor(private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService,
              private assetService: AssetService,
              private modalService: NgbModal,
              private router: Router) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.focus = store.lampsman.personInformationFocus;
    });
  }

  ngOnInit() {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 10
    };
  }

  ngOnChanges(changes: {[propertyName: string]: SimpleChange}): void {
    if (changes['personAssignments'] && this.personAssignments) {
        // this.reRenderDatatable();
    }
  }

  ngAfterViewInit() {
    // Calling the DT trigger to manually render the table
    this.dtTrigger.next();
  }

  viewDetails(asset: Asset) {
    const detailModalRef = this.modalService.open(AssetDetailsModalComponent);
    detailModalRef.componentInstance.asset = asset;
  }

  modify(asset: Asset) {
    const modifyModalRef = this.modalService.open((ModifyAssetModalComponent));
    modifyModalRef.componentInstance.asset = asset;
  }

  deallocate(assignment: Assignment) {
    const deallocateModalRef = this.modalService.open(DeallocateConfirmationModalComponent);
    deallocateModalRef.componentInstance.assignment = assignment;
    deallocateModalRef.result.then(result => {
      if (result) {
        const ass = {id: assignment.assignmentId, personId: assignment.person.personId};
        this.assetService.deassignAsset(ass)
          .subscribe(response => {
            console.log(response);
            // this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
            this.toastr.info('The asset has been deallocated', 'Asset Deassigned');
            // this.ngRedux.dispatch({type: ADD_RECENTLY_DEALLOCATED_ASSET, asset: assignment.asset});
            this.fetchAsset(assignment.asset);
            if (response._embedded) {
              this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
            } else {
              this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: []});
            }
          });
      }
    });
  }

  fetchAsset(asset: Asset) {
    this.assetService.getAssetByID(asset.assetId)
      .subscribe(response => {
        this.ngRedux.dispatch({type: ADD_RECENTLY_DEALLOCATED_ASSET, asset: response});
      });
  }
  repairHistory(asset: any) {
    this.router.navigate(['/reports/lms/repair', {asset: asset.assetId}]);
  }

  allocationHistory(asset: Asset) {
    this.router.navigate(['/reports/lms/allocation/history/asset', {asset: asset.assetId}]);
  }

  reRenderDatatable() {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      this.dtTrigger.next();
    });
  }
}
