import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonAllocationsComponent } from './person-allocations.component';

describe('PersonAllocationsComponent', () => {
  let component: PersonAllocationsComponent;
  let fixture: ComponentFixture<PersonAllocationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonAllocationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonAllocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
