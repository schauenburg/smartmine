import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecentAllocationsComponent } from './recent-allocations.component';

describe('RecentAllocationsComponent', () => {
  let component: RecentAllocationsComponent;
  let fixture: ComponentFixture<RecentAllocationsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecentAllocationsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecentAllocationsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
