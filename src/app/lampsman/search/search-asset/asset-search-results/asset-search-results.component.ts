import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { AssetSearchResultsDataSource } from './asset-search-results-datasource';
import {Asset} from '../../../../_models/asset';
import {
  ADD_RECENTLY_ALLOCATED_ASSET, ADD_RECENTLY_DEALLOCATED_ASSET, ADD_TO_ASSET_SEARCH_RESULTS, REMOVE_FROM_ASSET_SEARCH_RESULTS,
  UPDATE_ASSET_SEARCH_RESULTS,
  UPDATE_PERSON_ASSIGNMENTS, UPDATE_PERSON_INFORMATION_FOCUS,
  UPDATE_SELECTED_ASSET, UPDATE_SELECTED_PERSON
} from '../../../lampsman.action';
import {AssetDetailsModalComponent} from '../../../asset/asset-details-modal/asset-details-modal.component';
import {ModifyAssetModalComponent} from '../../../asset/modify-asset-modal/modify-asset-modal.component';
import {NgRedux} from '@angular-redux/store';
import {LookupService} from '../../../../_services/lookup.service';
import {IAppState} from '../../../../app.store';
import {ToastrService} from 'ngx-toastr';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Router} from '@angular/router';
import {PersonService} from '../../../../_services/person.service';
import {AssetService} from '../../../../_services/asset.service';
import {LocationService} from '../../../../_services/location.service';
import {Assignment} from '../../../../_models/assignment';
import {Person} from '../../../../_models/person';
import {DeallocateConfirmationModalComponent} from '../../../allocate/deallocate-confirmation-modal/deallocate-confirmation-modal.component';

@Component({
  selector: 'sm-asset-search-results',
  templateUrl: './asset-search-results.component.html',
  styleUrls: ['./asset-search-results.component.css']
})
export class AssetSearchResultsComponent implements OnInit {
  person: Person;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: AssetSearchResultsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['assetId', 'type', 'status', 'allocStatus', 'options'];

  constructor(private assetService: AssetService,
              private locationService: LocationService,
              private personService: PersonService,
              private lookupService: LookupService,
              private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService,
              private modalService: NgbModal,
              private router: Router) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.person = store.lampsman.selectedPerson;
    });
  }
  ngOnInit() {
    this.dataSource = new AssetSearchResultsDataSource(this.paginator, this.sort);
  }

  selectAsset(asset: Asset) {
    if (asset.allocStatus === 'allocated' || asset.allocStatus === 'temp-alloc') {
      this.ngRedux.dispatch({type: UPDATE_SELECTED_ASSET, selectedAsset: asset});
      this.findPersonForAsset(asset);
    }
  }

  findPersonForAsset(asset: Asset) {
    let assignment: Assignment;
    this.assetService.findAssignmentForAsset(asset.assetId)
      .subscribe(response => {
        assignment = response;
        if (assignment.person !== undefined) {
          this.selectPerson(assignment.person);
        }
      });
  }

  private selectPerson(person: Person) {
    this.ngRedux.dispatch({type: UPDATE_SELECTED_PERSON, selectedPerson: person});
    this.ngRedux.dispatch({type: UPDATE_PERSON_INFORMATION_FOCUS, focus: true});
    this.fetchPersonAssignments(person);
  }

  fetchPersonAssignments(person: Person) {
    this.personService.getAssignmentsForPerson(person.personId)
      .subscribe(response => {
        this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
      });
  }

  deallocate(asset: Asset) {
    if (asset.allocStatus === 'allocated' || asset.allocStatus === 'temp-alloc') {
      this.assetService.findAssignmentForAsset(asset.assetId)
        .subscribe(response => {
          this.openDeassignConfirmationModal(asset, response);
        });
    }
  }

  findAssetAssignment(asset: Asset): Assignment {
    let assignment: Assignment;
    this.assetService.findAssignmentForAsset(asset.assetId)
      .subscribe(response => {
        console.log(response);
        assignment = response;
      });
    return assignment;
  }

  openDeassignConfirmationModal(asset: Asset, assignment: Assignment) {
    const deallocateModalRef = this.modalService.open(DeallocateConfirmationModalComponent);
    deallocateModalRef.componentInstance.assignment = assignment;
    deallocateModalRef.result.then(result => {
      if (result) {
        const ass = {id: assignment.assignmentId, personId: assignment.person.personId};
        this.assetService.deassignAsset(ass)
          .subscribe(response => {
            // this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
            this.toastr.info('The asset has been deallocated', 'Asset Deassigned');
            // this.ngRedux.dispatch({type: ADD_RECENTLY_DEALLOCATED_ASSET, asset: asset});
            // this.fetchAsset(asset);
            this.allocate(asset);
          });
      }
    });
  }

  fetchAsset(asset: Asset) {
    this.assetService.getAssetByID(asset.assetId)
      .subscribe(response => {
        console.log(response);
        this.ngRedux.dispatch({type: ADD_RECENTLY_DEALLOCATED_ASSET, asset: response});
      });
  }

  allocate(asset: Asset) {
    if (this.person) {
      const ass = {assetId: asset.assetId, personId: this.person.personId, status: 'allocated'};
      this.assetService.assignAsset(ass)
        .subscribe(response => {
          const newAssignments = response._embedded['assignmentResources'];
          this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: newAssignments});
          this.toastr.success('Asset was allocated successfully', 'Allocation Successful!');
          const updatedAssignment = newAssignments.find(x => x.asset.assetId === asset.assetId);
          this.ngRedux.dispatch({type: ADD_RECENTLY_ALLOCATED_ASSET, asset: updatedAssignment.asset});
          this.ngRedux.dispatch({type: REMOVE_FROM_ASSET_SEARCH_RESULTS, asset: updatedAssignment.asset});
          this.ngRedux.dispatch({type: ADD_TO_ASSET_SEARCH_RESULTS, asset: updatedAssignment.asset});
        });
    } else {
      this.toastr.warning('To assign an asset, please choose a person to assign to first', 'No Person Selected');
    }
  }

  viewDetails(asset: Asset) {
    let detailModalRef: any;
    if (asset.allocStatus === 'allocated' || asset.allocStatus === 'temp-alloc') {
      this.assetService.findAssignmentForAsset(asset.assetId)
        .subscribe(response => {
          console.log(response);
          detailModalRef = this.modalService.open(AssetDetailsModalComponent);
          detailModalRef.componentInstance.asset = asset;
          detailModalRef.componentInstance.assignment = response;
        });
    } else {
      detailModalRef = this.modalService.open(AssetDetailsModalComponent);
      detailModalRef.componentInstance.asset = asset;
    }

    // detailModalRef.componentInstance.assignment = this.findAssetAssignment(asset);
    /*    if (assign !== undefined) {
          detailModalRef.componentInstance.assignment = assign;
        }*/
  }

  modify(asset: Asset) {
    const modifyModalRef = this.modalService.open((ModifyAssetModalComponent));
    modifyModalRef.componentInstance.asset = asset;
  }

  repairHistory(asset: Asset) {
    this.router.navigate(['/reports/lms/repair', {asset: asset.assetId}]);
  }

  allocationHistory(asset: Asset) {
    this.router.navigate(['/reports/lms/allocation/history/asset', {asset: asset.assetId}]);
  }
}
