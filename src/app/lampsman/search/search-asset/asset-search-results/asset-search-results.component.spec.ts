
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssetSearchResultsComponent } from './asset-search-results.component';

describe('AssetSearchResultsComponent', () => {
  let component: AssetSearchResultsComponent;
  let fixture: ComponentFixture<AssetSearchResultsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ AssetSearchResultsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AssetSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
