import {AfterViewInit, Component, OnInit} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {NgRedux, select} from '@angular-redux/store';
import {Observable} from 'rxjs';
import {Person} from '../../../_models/person';
import {PersonService} from '../../../_services/person.service';
import {IAppState} from '../../../app.store';
import {
  SHOW_PERSONSEARCH_RESULTS, UPDATE_PERSON_ASSIGNMENTS, UPDATE_PERSON_INFORMATION_FOCUS, UPDATE_PERSONSEARCH_RESULTS,
  UPDATE_SELECTED_PERSON
} from '../../lampsman.action';
import {IPersonType} from '../../../_models/person-type';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'sm-search-person',
  templateUrl: './search-person.component.html',
  styleUrls: ['./search-person.component.css']
})
export class SearchPersonComponent implements OnInit, AfterViewInit {
  showSearchResults = false;
  simpleView = true;
  advancedView = false;

  @select(s => s.lampsman.personSearchResults) searchResults: Observable<Person[]>;
  @select(s => s.lampsman.personTypes) personTypes: Observable<IPersonType>;

  searchPersonForm: FormGroup;

  constructor(private personService: PersonService,
              private ngRedux: NgRedux<IAppState>,
              private toastr: ToastrService) {
    this.ngRedux.subscribe(() => {
      const store: any = this.ngRedux.getState();
      this.showSearchResults = store.lampsman.showPersonSearchResults;
    });
  }

  ngOnInit() {
    this.personService.fetchPersonTypes();
    this.initFormModel();
  }

  ngAfterViewInit() {
    // this.dtTrigger.next();
  }

  initFormModel() {
    this.searchPersonForm = new FormGroup({
      name: new FormControl(),
      personType: new FormControl(),
      companyNo: new FormControl(),
      identityNo: new FormControl()
    });
  }

  searchPerson(form: any) {
    this.personService.searchPerson(form)
      .subscribe(response => {
        const result = response._embedded['personResources'];
          if (result && result.length > 1) {
            this.ngRedux.dispatch({type: UPDATE_PERSONSEARCH_RESULTS, personSearchResults: result});
            this.ngRedux.dispatch({type: SHOW_PERSONSEARCH_RESULTS, show: true});
          }
          if (result.length === 1) {
            this.ngRedux.dispatch({type: UPDATE_SELECTED_PERSON, selectedPerson: result[0]});
            this.ngRedux.dispatch({type: UPDATE_PERSON_INFORMATION_FOCUS, focus: true});
            this.fetchPersonAssignments(result[0]);
          }
        },
        error2 => {
          if (error2.error.code === 404) {
            this.toastr.warning(error2.error.message, 'Person Not Found');
          } else {
            this.toastr.error('There was an error searching person!', 'Search Error ' + error2.error.status);
          }
        });
  }

  toggleSimpleAdvanced() {
    this.simpleView = !this.simpleView;
    this.advancedView = !this.advancedView;
  }

  fetchPersonAssignments(person: Person) {
    this.personService.getAssignmentsForPerson(person.personId)
      .subscribe(response => {
        if (response._embedded) {
          this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
        } else {
          this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: []});
        }
      });
  }
}
