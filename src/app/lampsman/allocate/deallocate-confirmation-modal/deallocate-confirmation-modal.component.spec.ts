import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeallocateConfirmationModalComponent } from './deallocate-confirmation-modal.component';

describe('DeallocateConfirmationModalComponent', () => {
  let component: DeallocateConfirmationModalComponent;
  let fixture: ComponentFixture<DeallocateConfirmationModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeallocateConfirmationModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeallocateConfirmationModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
