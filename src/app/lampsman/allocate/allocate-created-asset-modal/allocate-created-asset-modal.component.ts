import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Person} from '../../../_models/person';
import {Asset} from '../../../_models/asset';
import {AssetService} from '../../../_services/asset.service';
import {Assignment} from '../../../_models/assignment';
import {ToastrService} from 'ngx-toastr';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../../app.store';
import {UPDATE_PERSON_ASSIGNMENTS} from '../../lampsman.action';

@Component({
  selector: 'sm-allocate-created-asset-modal',
  templateUrl: './allocate-created-asset-modal.component.html',
  styleUrls: ['./allocate-created-asset-modal.component.css']
})
export class AllocateCreatedAssetModalComponent implements OnInit {
  @Input() asset: Asset;
  @Input() person: Person;

  constructor(public activeModal: NgbActiveModal,
              private assetService: AssetService,
              private toastr: ToastrService,
              private ngRedux: NgRedux<IAppState>) { }

  ngOnInit() {
  }

  allocate() {
    const assignment = {
      assetId: this.asset.assetId,
      personId: this.person.personId,
      status: 'allocated'
    };

    this.assetService.assignAsset(assignment)
      .subscribe(response => {
        console.log(response);
        this.toastr.success(this.asset.type.description + ' ' + this.asset.assetId +
          ' was successfully assigned to ' + this.person.personId, 'Success!');
        this.ngRedux.dispatch({type: UPDATE_PERSON_ASSIGNMENTS, assignments: response._embedded['assignmentResources']});
        this.activeModal.close('success');
      });
  }
}
