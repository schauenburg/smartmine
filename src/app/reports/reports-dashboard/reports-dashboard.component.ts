import { Component, OnInit } from '@angular/core';
import {ReportService} from '../../_services/report.service';

@Component({
  selector: 'sm-reports-dashboard',
  templateUrl: './reports-dashboard.component.html',
  styleUrls: ['./reports-dashboard.component.css']
})
export class ReportsDashboardComponent implements OnInit {
  categories: string[];

  constructor(private reportService: ReportService) { }

  ngOnInit() {
    this.reportService.getReportCategories()
      .subscribe(response => {
        this.categories = response;
      });
  }

}
