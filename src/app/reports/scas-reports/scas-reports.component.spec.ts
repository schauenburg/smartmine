import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ScasReportsComponent } from './scas-reports.component';

describe('ScasReportsComponent', () => {
  let component: ScasReportsComponent;
  let fixture: ComponentFixture<ScasReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ScasReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ScasReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
