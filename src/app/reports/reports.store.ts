import {tassign} from 'tassign';
import {REPORT_CATEGORIES, UPDATE_SELECTED_REPORT} from './reports.action';
import {ReportConfig} from '../_models/report-config';

export interface IReportsState {
  categories: string[];
  selectedReport: ReportConfig;
}
export const REPORTS_INITIAL_STATE: IReportsState = {
  categories: [],
  selectedReport: undefined
};

function fetchReportCategories(state: IReportsState, action: any): IReportsState {
  return tassign(state, {categories: action.categories});
}

function updateSelectedReport(state: IReportsState, action: any): IReportsState {
  return tassign(state, {selectedReport: action.report});
}

export function reportReducer(state: IReportsState = REPORTS_INITIAL_STATE, action: any): IReportsState {
  switch (action.type) {
    case REPORT_CATEGORIES:
      return fetchReportCategories(state, action);
    case UPDATE_SELECTED_REPORT:
      return updateSelectedReport(state, action);
    default:
      return state;
  }
}
