import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LmsReportsComponent } from './lms-reports.component';

describe('LmsReportsComponent', () => {
  let component: LmsReportsComponent;
  let fixture: ComponentFixture<LmsReportsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LmsReportsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LmsReportsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
