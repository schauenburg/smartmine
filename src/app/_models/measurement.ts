import {Asset} from './asset';
import {IAttr} from './IAttr';

export interface Measurement {
  agent: any;
  assetMeasurementRoles: any[];
  asset: Asset;
  location: any;
  measurementTime: any;
  personMeasurementRoles: any[];
  uuid: string;
  validMeasurement: boolean;
  value: any;
  type: any;
  attr: IAttr[];
}
