import {Asset} from './asset';
import {Person} from './person';

export class Assignment {
  assignmentId: number;
  asset: Asset;
  person: Person;
  status: string;
  startDate: any;
  enabled: boolean;

}
