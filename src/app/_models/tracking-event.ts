import {Asset} from './asset';
import {Person} from './person';

export interface TrackingEvent {
  agent: any;
  eventTime: any;
  location: any;
  uuid: string;
  type: any;
  asset: Asset;
  person: Person;
  status: string;
}
