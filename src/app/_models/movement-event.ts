import {Asset} from './asset';
import {Person} from './person';

export class MovementEvent {
  assets: Asset[];
  person: Person;
  eventTime: any;
  startLocation: any;
  endLocation: any;
  trackingEvents: any[];
  type: any;
  uuid: string;
}
