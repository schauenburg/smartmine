export interface ISelectItem {
  description: string;
  label: string;
  value: any;
  escape: boolean;
  noSelectionOption: boolean;
}
