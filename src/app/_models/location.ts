import {IAttributeMap} from './attribute-map';
import {ILocationType} from './location-type';

export class Location {
  id: number;
  parentId: number;
  name: string;
  description: string;
  type: ILocationType;
  startDate: any;
  lastUpdate: any;
  attributeMap: IAttributeMap;
  attr: any[];
}
