import {ReportConfig} from './report-config';

export interface ReportOutputContext {
  criteria: any;
  documentRendered: boolean;
  output: any;
  parameters: any[];
  report: ReportConfig;
  reportExecutionActive: boolean;
  reportExecutionMsg: string;
  reportTime: any;
  results: any[];
  resultsPresent: boolean;
  scheduleHour: number;
  scheduleMin: number;
  userTextList: string[];
}
