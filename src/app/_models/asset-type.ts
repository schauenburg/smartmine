import {IAttribute} from './attribute';

export interface IAssetType {
  type: string;
  name: string;
  description: string;
  attributes: IAttribute[];
  properties: any[];
  statusLookup: string;
  assetClass: any;
  component: any;
}
