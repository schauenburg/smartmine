import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {EventsComponent} from './events/events.component';
import {TrackingEventsComponent} from './tracking-events/tracking-events.component';
import {MeasurementEventsComponent} from './measurement-events/measurement-events.component';
import {MovementEventsComponent} from './movement-events/movement-events.component';

const eventRoutes: Routes = [
  {
    path: 'events',
    component: EventsComponent,
    children: [
      {
        path: 'tracking',
        component: TrackingEventsComponent
      },
      {
        path: 'measurement',
        component: MeasurementEventsComponent
      },
      {
        path: 'movement',
        component: MovementEventsComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(eventRoutes)],
  exports: [RouterModule]
})
export class EventsRoutingModule { }
