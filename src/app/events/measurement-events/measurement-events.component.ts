import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MeasurementService} from '../../_services/measurement.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AssetSearchModalComponent} from '../../lampsman/asset/asset-search-modal/asset-search-modal.component';
import {NgRedux} from '@angular-redux/store';
import {IAppState} from '../../app.store';
import {UPDATE_MEASUREMENTS} from '../events.actions';

@Component({
  selector: 'sm-measurement-events',
  templateUrl: './measurement-events.component.html',
  styleUrls: ['./measurement-events.component.css']
})
export class MeasurementEventsComponent implements OnInit {
  searchMeasurementsForm: FormGroup;
  startTimeOptions: any[];
  measurementTypes: any[];

  constructor(private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private measurementService: MeasurementService,
              private ngRedux: NgRedux<IAppState>) {
  }

  ngOnInit() {
    this.initSearchMeasurementsForm();
    this.getMeasurementTypes();
  }

  private getMeasurementTypes() {
    this.measurementService.getMeasurementTypes()
      .subscribe(response => {
        this.measurementTypes = response;
      });
  }

  private initSearchMeasurementsForm() {
    this.searchMeasurementsForm = this.formBuilder.group({
      measurementType: [''],
      startTime: [''],
      endTime: [''],
      assetId: [''],
      maxResults: ['200']
    });
  }

  searchMeasurements(form: any) {
    this.measurementService.searchMeasurements(form)
      .subscribe(response => {
        this.ngRedux.dispatch({type: UPDATE_MEASUREMENTS, measurements: response._embedded['measurementResources']});
      });
  }

  onTimeOptionChanged(option: any) {
  }

  openAssetSearchModal() {
    const assetSearchModalRef = this.modalService.open(AssetSearchModalComponent, {size: 'lg'});
    assetSearchModalRef.result.then(result => {
      if (result.success) {
        this.searchMeasurementsForm.controls['assetId'].setValue(result.assetId);
      }
    });
  }

}
