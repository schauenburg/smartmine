
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { MeasurementResultsComponent } from './measurement-results.component';

describe('MeasurementResultsComponent', () => {
  let component: MeasurementResultsComponent;
  let fixture: ComponentFixture<MeasurementResultsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MeasurementResultsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MeasurementResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
