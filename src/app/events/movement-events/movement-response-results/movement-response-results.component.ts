import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MovementResponseResultsDataSource } from './movement-response-results-datasource';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MovementResponse} from '../../../_models/movement-response';
import {MovementResponseDetailComponent} from '../movement-response-detail/movement-response-detail.component';

@Component({
  selector: 'sm-movement-response-results',
  templateUrl: './movement-response-results.component.html',
  styleUrls: ['./movement-response-results.component.css']
})
export class MovementResponseResultsComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MovementResponseResultsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['time', 'type', 'responseCode', 'responseMessage', 'personId', 'name', 'assetId', 'location', 'options'];

  constructor(private modalService: NgbModal,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.dataSource = new MovementResponseResultsDataSource(this.paginator, this.sort);
  }

  ngAfterViewInit() {
    this.cd.detectChanges();
  }

  viewDetails(response: MovementResponse) {
    const movementResponseDetailModalRef = this.modalService.open(MovementResponseDetailComponent);
    movementResponseDetailModalRef.componentInstance.response = response;
  }
}
