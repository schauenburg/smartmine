import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementEventsComponent } from './movement-events.component';

describe('MovementEventsComponent', () => {
  let component: MovementEventsComponent;
  let fixture: ComponentFixture<MovementEventsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementEventsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MovementEventsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
