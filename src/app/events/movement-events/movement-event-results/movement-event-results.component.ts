import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { MovementEventResultsDataSource } from './movement-event-results-datasource';
import {MovementEvent} from '../../../_models/movement-event';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MovementEventDetailComponent} from '../movement-event-detail/movement-event-detail.component';

@Component({
  selector: 'sm-movement-event-results',
  templateUrl: './movement-event-results.component.html',
  styleUrls: ['./movement-event-results.component.css']
})
export class MovementEventResultsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MovementEventResultsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['time', 'type', 'personId', 'name', 'startLocation', 'endLocation', 'options'];

  constructor(private modalService: NgbModal) {}

  ngOnInit() {
    this.dataSource = new MovementEventResultsDataSource(this.paginator, this.sort);
  }

  viewDetails(event: MovementEvent) {
    const movementEventDetailModalRef = this.modalService.open(MovementEventDetailComponent);
    movementEventDetailModalRef.componentInstance.event = event;
  }
}
