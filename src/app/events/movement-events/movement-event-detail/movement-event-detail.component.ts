import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MovementEvent} from '../../../_models/movement-event';

@Component({
  selector: 'sm-movement-event-detail',
  templateUrl: './movement-event-detail.component.html',
  styleUrls: ['./movement-event-detail.component.css']
})
export class MovementEventDetailComponent implements OnInit {
  @Input() event: MovementEvent;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
