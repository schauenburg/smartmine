import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup} from '@angular/forms';
import {MovementService} from '../../_services/movement.service';
import {LocationService} from '../../_services/location.service';
import {PersonSearchModalComponent} from '../../lampsman/person/person-search-modal/person-search-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrService} from 'ngx-toastr';
import {NgRedux, select} from '@angular-redux/store';
import {IAppState} from '../../app.store';
import {
  UPDATE_MVT_EVENTS,
  UPDATE_MVT_REQUESTS,
  UPDATE_MVT_RESPONSES, UPDATE_SHOW_MVT_EVENTS,
  UPDATE_SHOW_MVT_REQUESTS,
  UPDATE_SHOW_MVT_RESPONSES
} from '../events.actions';
import {Observable} from 'rxjs/Observable';

@Component({
  selector: 'sm-movement-events',
  templateUrl: './movement-events.component.html',
  styleUrls: ['./movement-events.component.css']
})
export class MovementEventsComponent implements OnInit {
  searchMovementForm: FormGroup;
  movementTypes: any[];
  startTimeOptions: any[];
  locations: any[];
  resultsTitle = 'MOVEMENT EVENT SEARCH RESULTS';
  @select(s => s.events.showMovementRequests) showRequests: Observable<boolean>;
  @select(s => s.events.showMovementResponses) showResponses: Observable<boolean>;
  @select(s => s.events.showMovementEvents) showEvents: Observable<boolean>;

  constructor(private formBuilder: FormBuilder,
              private modalService: NgbModal,
              private movementService: MovementService,
              private locationService: LocationService,
              private toastr: ToastrService,
              private ngRedux: NgRedux<IAppState>) {
  }

  ngOnInit() {
    this.initSearchMovementForm();
    this.getMovementTypes();
    this.getLocations();
  }

  private getMovementTypes() {
    this.movementService.getMovementTypes()
      .subscribe(response => {
        this.movementTypes = response;
      });
  }

  private initSearchMovementForm() {
    this.searchMovementForm = this.formBuilder.group({
      movementType: ['[event]'],
      location: [''],
      startTime: [''],
      endTime: [''],
      personId: [''],
      maxResults: ['200']
    });
  }

  private getLocations() {
    this.locationService.listAllLocations()
      .subscribe(response => {
        this.locations = response;
      });
  }

  searchMovement(form: any) {
    this.movementService.searchMovements(form)
      .subscribe(response => {
          if (response._embedded) {
            if (response._embedded['movementResponseResources']) {
              this.ngRedux.dispatch({type: UPDATE_MVT_RESPONSES, responses: response._embedded['movementResponseResources']});
              this.ngRedux.dispatch({type: UPDATE_SHOW_MVT_RESPONSES, show: true});
              this.resultsTitle = 'MOVEMENT RESPONSES';
            }
            if (response._embedded['movementRequestResources']) {
              this.ngRedux.dispatch({type: UPDATE_MVT_REQUESTS, requests: response._embedded['movementRequestResources']});
              this.ngRedux.dispatch({type: UPDATE_SHOW_MVT_REQUESTS, show: true});
              this.resultsTitle = 'MOVEMENT REQUESTS';
            }
            if (response._embedded['movementEventResources']) {
              this.ngRedux.dispatch({type: UPDATE_MVT_EVENTS, events: response._embedded['movementEventResources']});
              this.ngRedux.dispatch({type: UPDATE_SHOW_MVT_EVENTS, show: true});
              this.resultsTitle = 'MOVEMENT EVENTS';
            }
          } else {
            this.ngRedux.dispatch({type: UPDATE_MVT_RESPONSES, responses: []});
            this.ngRedux.dispatch({type: UPDATE_SHOW_MVT_RESPONSES, show: false});
            this.ngRedux.dispatch({type: UPDATE_MVT_REQUESTS, requests: []});
            this.ngRedux.dispatch({type: UPDATE_SHOW_MVT_REQUESTS, show: false});
            this.ngRedux.dispatch({type: UPDATE_MVT_EVENTS, events: []});
            this.ngRedux.dispatch({type: UPDATE_SHOW_MVT_EVENTS, show: false});
            this.resultsTitle = 'NO MOVEMENT RESULTS';
          }
        },
        error2 => {
          console.log(error2);
          if (error2.error.code === 404) {
            this.toastr.warning(error2.error.message, 'Movements Not Found');
          } else {
            this.toastr.error('There was an error searching movements!', 'Search Error ' + error2.error.status);
          }
        });
  }

  openPersonSearchModal() {
    const personSearchModalRef = this.modalService.open(PersonSearchModalComponent, {size: 'lg'});

    personSearchModalRef.result.then(result => {
      if (result.success) {
        this.searchMovementForm.controls['personId'].setValue(result.personId);
      }
    });
  }

  onTypeChanged(option: any) {
    console.log(option);
  }
}
