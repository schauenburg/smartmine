
import { fakeAsync, ComponentFixture, TestBed } from '@angular/core/testing';

import { MovementRequestResultsComponent } from './movement-request-results.component';

describe('MovementRequestResultsComponent', () => {
  let component: MovementRequestResultsComponent;
  let fixture: ComponentFixture<MovementRequestResultsComponent>;

  beforeEach(fakeAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ MovementRequestResultsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(MovementRequestResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should compile', () => {
    expect(component).toBeTruthy();
  });
});
