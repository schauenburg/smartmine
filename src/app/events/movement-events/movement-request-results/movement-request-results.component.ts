import {AfterViewInit, ChangeDetectorRef, Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSort} from '@angular/material';
import {MovementRequestResultsDataSource} from './movement-request-results-datasource';
import {MovementRequest} from '../../../_models/movement-request';
import {MovementRequestDetailComponent} from '../movement-request-detail/movement-request-detail.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'sm-movement-request-results',
  templateUrl: './movement-request-results.component.html',
  styleUrls: ['./movement-request-results.component.css']
})
export class MovementRequestResultsComponent implements OnInit, AfterViewInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: MovementRequestResultsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['time', 'type', 'personId', 'name', 'location', 'options'];

  constructor(private modalService: NgbModal,
              private cd: ChangeDetectorRef) {
  }

  ngOnInit() {
    this.dataSource = new MovementRequestResultsDataSource(this.paginator, this.sort);
  }

  ngAfterViewInit() {
    this.cd.detectChanges();
  }

  viewDetails(request: MovementRequest) {
    console.log(request);
    const movementRequestDetailModalRef = this.modalService.open(MovementRequestDetailComponent);
    movementRequestDetailModalRef.componentInstance.request = request;
  }
}
