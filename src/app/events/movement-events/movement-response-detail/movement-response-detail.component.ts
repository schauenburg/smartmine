import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {MovementResponse} from '../../../_models/movement-response';

@Component({
  selector: 'sm-movement-response-detail',
  templateUrl: './movement-response-detail.component.html',
  styleUrls: ['./movement-response-detail.component.css']
})
export class MovementResponseDetailComponent implements OnInit {

  @Input() response: MovementResponse;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
