import {Component, Input, OnInit} from '@angular/core';
import {MovementRequest} from '../../../_models/movement-request';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'sm-movement-request-detail',
  templateUrl: './movement-request-detail.component.html',
  styleUrls: ['./movement-request-detail.component.css']
})
export class MovementRequestDetailComponent implements OnInit {
  @Input() request: MovementRequest;
  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
