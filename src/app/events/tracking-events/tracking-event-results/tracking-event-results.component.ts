import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator, MatSort } from '@angular/material';
import { TrackingEventResultsDataSource } from './tracking-event-results-datasource';

@Component({
  selector: 'sm-tracking-event-results',
  templateUrl: './tracking-event-results.component.html',
  styleUrls: ['./tracking-event-results.component.css']
})
export class TrackingEventResultsComponent implements OnInit {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource: TrackingEventResultsDataSource;

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['time', 'eventType', 'assetType', 'assetId', 'personId', 'location'];

  ngOnInit() {
    this.dataSource = new TrackingEventResultsDataSource(this.paginator, this.sort);
  }
}
