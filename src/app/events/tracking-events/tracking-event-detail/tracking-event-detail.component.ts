import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'sm-tracking-event-detail',
  templateUrl: './tracking-event-detail.component.html',
  styleUrls: ['./tracking-event-detail.component.css']
})
export class TrackingEventDetailComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
