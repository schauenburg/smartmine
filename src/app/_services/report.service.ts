import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {ReportConfig} from '../_models/report-config';

@Injectable({
  providedIn: 'root'
})
export class ReportService {
  private reportBaseUrl = environment.api.base + 'report/';

  constructor(private http: HttpClient) {
  }

  getReportCategories(): Observable<string[]> {
    return this.http.get<any>(this.reportBaseUrl + 'category');
  }

  getReportsByCategory(category: string): Observable<ReportConfig[]> {
    return this.http.get<any>(this.reportBaseUrl + category + '/reports');
  }

  getReportByName(name: string): Observable<ReportConfig> {
    return this.http.get<any>(this.reportBaseUrl + name);
  }

  executeReport(data: any): Observable<any> {
    return this.http.post(this.reportBaseUrl + 'execute', data);
  }

  fetchReportPDF(data: any) {
    return this.http.post(this.reportBaseUrl + 'pdf', data, {responseType: 'blob'});
  }
}
