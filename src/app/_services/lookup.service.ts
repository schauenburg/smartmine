import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ISelectItem} from '../_models/select-item';
import {Observable} from 'rxjs';
import {environment} from '../../environments/environment';

@Injectable()
export class LookupService {
  private lookupBaseUrl = environment.api.base + 'lookup/';

  constructor(private http: HttpClient) { }

  getLookupList(name: string): Observable<ISelectItem[]> {
    return this.http.get<any>(this.lookupBaseUrl + name);
  }
}
