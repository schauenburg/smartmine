import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../environments/environment';
import {Observable} from 'rxjs/index';
import {ISelectItem} from '../_models/select-item';
import {ResultList} from '../_models/result-list';
import {Measurement} from '../_models/measurement';

@Injectable({
  providedIn: 'root'
})
export class MeasurementService {
  private measurementBaseUrl = environment.api.base + 'measurement/';

  constructor(private http: HttpClient) { }

  getMeasurementTypes(): Observable<ISelectItem[]> {
    return this.http.get<any>(this.measurementBaseUrl + 'types');
  }

  searchMeasurements(data: any): Observable<ResultList<Measurement>> {
    return this.http.post<any>(this.measurementBaseUrl + 'search', data);
  }
}
