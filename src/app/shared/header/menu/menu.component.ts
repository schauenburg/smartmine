import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sm-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {
  menuTitle: string;
  constructor() { }

  ngOnInit() {
  }

  setMenuTitle(title: string) {
    this.menuTitle = title;
  }

}
