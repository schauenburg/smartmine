import {Component, OnDestroy, OnInit} from '@angular/core';
import {SystemService} from '../../../_services/system.service';

@Component({
  selector: 'sm-version',
  templateUrl: './version.component.html',
  styleUrls: ['./version.component.css']
})
export class VersionComponent implements OnInit, OnDestroy {
  systemDetails = {
    version: '1.0.0',
    organisation: 'Schauenburg',
    applicationName: 'Smart Mine',
    lastUpdated: Date.now()
  };
  sub: any;

  constructor(private systemService: SystemService) { }

  ngOnInit() {
    this.sub = this.systemService.getSystemDetails()
      .subscribe(response => {
        this.systemDetails = response;
      });
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
