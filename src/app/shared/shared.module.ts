///<reference path="../../../node_modules/@angular/forms/src/form_providers.d.ts"/>
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { LogoComponent } from './header/logo/logo.component';
import { MenuComponent } from './header/menu/menu.component';
import { NotificationsComponent } from './header/notifications/notifications.component';
import { UserComponent } from './header/user/user.component';
import { VersionComponent } from './header/version/version.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {RouterModule} from '@angular/router';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NgReduxModule} from '@angular-redux/store';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ToastrModule} from 'ngx-toastr';
import {DataTablesModule} from 'angular-datatables';
import {AttributeTransformPipe} from '../_pipes/attribute-transform.pipe';
import {SortPipe} from '../_pipes/sort.pipe';
import {DateValueAccessorModule} from 'angular-date-value-accessor';
import {NgPipesModule} from 'ngx-pipes';
import {MapPipe} from '../_pipes/map.pipe';
import {AssetTypeBoardComponent} from './sideboards/asset-type-board/asset-type-board.component';
import {AssignmentsBoardComponent} from './sideboards/assignments-board/assignments-board.component';
import {LastSeenBoardComponent} from './sideboards/last-seen-board/last-seen-board.component';
import {MovementsBoardComponent} from './sideboards/movements-board/movements-board.component';
import {MeasurementsBoardComponent} from './sideboards/measurements-board/measurements-board.component';
import {AssetSummaryModalComponent} from './sideboards/asset-summary-modal/asset-summary-modal.component';
import {AssetTypeSummaryModalComponent} from './sideboards/asset-type-summary-modal/asset-type-summary-modal.component';
import {MatInputModule, MatPaginatorModule, MatProgressSpinnerModule, MatSortModule, MatTableModule} from '@angular/material';
import {AssetSearchModalComponent} from '../lampsman/asset/asset-search-modal/asset-search-modal.component';
import {PersonSearchModalComponent} from '../lampsman/person/person-search-modal/person-search-modal.component';
import {AssetSearchResultsComponent} from '../lampsman/search/search-asset/asset-search-results/asset-search-results.component';

const ANGULAR_MODULES: any[] = [
  FormsModule, ReactiveFormsModule, HttpClientModule, RouterModule, BrowserAnimationsModule
];

const REDUX_MODULES: any[] = [
  NgReduxModule,
];

const MATERIAL_MODULES: any[] = [
  MatInputModule,
  MatTableModule,
  MatPaginatorModule,
  MatSortModule,
  MatProgressSpinnerModule
];

@NgModule({
  imports: [
    CommonModule,
    ANGULAR_MODULES,
    REDUX_MODULES,
    MATERIAL_MODULES,
    NgbModule.forRoot(),
    ToastrModule.forRoot({
      timeOut: 10000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
      progressBar: true
    }),
    DataTablesModule.forRoot(),
    DateValueAccessorModule,
    NgPipesModule
  ],
  declarations: [
    HeaderComponent,
    FooterComponent,
    LogoComponent,
    MenuComponent,
    NotificationsComponent,
    UserComponent,
    VersionComponent,
    AssetTypeBoardComponent,
    AssignmentsBoardComponent,
    LastSeenBoardComponent,
    MovementsBoardComponent,
    MeasurementsBoardComponent,
    AssetSummaryModalComponent,
    AssetTypeSummaryModalComponent,
    AssetSearchModalComponent,
    PersonSearchModalComponent,
    AssetSearchResultsComponent,
    AttributeTransformPipe,
    MapPipe,
    SortPipe
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    LogoComponent,
    MenuComponent,
    NotificationsComponent,
    UserComponent,
    VersionComponent,
    AssetTypeBoardComponent,
    AssignmentsBoardComponent,
    LastSeenBoardComponent,
    MovementsBoardComponent,
    MeasurementsBoardComponent,
    AssetSearchResultsComponent,
    AttributeTransformPipe,
    SortPipe,
    MapPipe,
    ANGULAR_MODULES,
    REDUX_MODULES,
    MATERIAL_MODULES,
    DataTablesModule,
    NgbModule,
    NgPipesModule
  ],
  entryComponents: [
    AssetSummaryModalComponent,
    AssetTypeSummaryModalComponent,
    AssetSearchModalComponent,
    PersonSearchModalComponent
  ]
})
export class SharedModule { }
