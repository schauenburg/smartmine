import { Component, OnInit } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'sm-asset-summary-modal',
  templateUrl: './asset-summary-modal.component.html',
  styleUrls: ['./asset-summary-modal.component.css']
})
export class AssetSummaryModalComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
