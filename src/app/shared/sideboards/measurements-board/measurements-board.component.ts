import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sm-measurements-board',
  templateUrl: './measurements-board.component.html',
  styleUrls: ['./measurements-board.component.css']
})
export class MeasurementsBoardComponent implements OnInit {
  measurement = false;
  measurements: any[];
  constructor() { }

  ngOnInit() {
  }

}
