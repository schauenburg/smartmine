import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'sm-assignments-board',
  templateUrl: './assignments-board.component.html',
  styleUrls: ['./assignments-board.component.css']
})
export class AssignmentsBoardComponent implements OnInit {
  assignment = false;
  assignmentStatistics: any[];
  constructor() { }

  ngOnInit() {
  }

}
