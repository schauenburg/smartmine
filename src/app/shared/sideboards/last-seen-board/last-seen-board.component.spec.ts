import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LastSeenBoardComponent } from './last-seen-board.component';

describe('LastSeenBoardComponent', () => {
  let component: LastSeenBoardComponent;
  let fixture: ComponentFixture<LastSeenBoardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LastSeenBoardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LastSeenBoardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
