import {combineReducers} from 'redux';
import {ILampsmanState, LAMPSMAN_INITIAL_STATE, lampsmanReducer} from './lampsman/lampsman.store';
import {IUserState, USER_INITIAL_STATE, userReducer} from './users/users.store';
import {IReportsState, reportReducer, REPORTS_INITIAL_STATE} from './reports/reports.store';
import {eventReducer, EVENTS_INITIAL_STATE, IEventsState} from './events/events.store';

export interface IAppState {
  lampsman: ILampsmanState;
  user: IUserState;
  reports: IReportsState;
  events: IEventsState;
}

export const INITIAL_STATE: IAppState = {
  lampsman: LAMPSMAN_INITIAL_STATE,
  user: USER_INITIAL_STATE,
  reports: REPORTS_INITIAL_STATE,
  events: EVENTS_INITIAL_STATE
};

export const rootReducer: any = combineReducers({
  lampsman: lampsmanReducer,
  user: userReducer,
  reports: reportReducer,
  events: eventReducer
});
